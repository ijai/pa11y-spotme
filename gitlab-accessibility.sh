#!/usr/bin/env sh
mkdir -p reports

pa11y-ci -j --config "/pa11y-configs/.pa11yci" > reports/gl-accessibility.json

for url in $@
do
	
	if [ "$url" = "https://app.osdec.gov.my/dashboard/pegawai" ]
	then
		echo testing dashboard
		filename="reports/$(echo $url | sed -E 's/^https?:\/\///' | sed -E 's/\//-/g')-accessibility.html"
		pa11y --config "/pa11y-configs/pa11y-dashboard.json" $url | tee $filename > /dev/null
	fi

	if [ "$url" = "https://app.osdec.gov.my/auth/login" ]
	then
		echo testing login
		filename="reports/$(echo $url | sed -E 's/^https?:\/\///' | sed -E 's/\//-/g')-accessibility.html"
		pa11y --config "/pa11y-configs/pa11y.json" $url | tee $filename > /dev/null
	fi

	
done
